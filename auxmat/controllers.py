#coding: utf-8
"""This module contains the controller classes of the application."""

# symbols which are imported by "from auxmat.controllers import *"
__all__ = ['Root']

# standard library imports
# import logging
import datetime
from datetime import time

# third-party imports
from cherrypy import request
from turbogears import controllers, expose, flash, identity, redirect, visit
from auxmat import model
from copy import copy

# project specific imports
# from auxmat import model
# from auxmat import jsonify


# log = logging.getLogger('auxmat.controllers')


class Root(controllers.RootController):
    """The root controller of the application."""

    @expose()
    @identity.require(identity.not_anonymous())
    def index(self):
        raise redirect("/lista_blocos")

    @expose('auxmat.templates.new_user')
    def new_user(self, *args, **kw):
        """Show the login form or forward user to previously requested page."""



        return dict()

    @expose('auxmat.templates.login')
    def login(self, forward_url=None, *args, **kw):
        """Show the login form or forward user to previously requested page."""

        if forward_url:
            if isinstance(forward_url, list):
                forward_url = forward_url.pop(0)
            else:
                del request.params['forward_url']

        new_visit = visit.current()
        if new_visit:
            new_visit = new_visit.is_new

        if (not new_visit and not identity.current.anonymous
                and identity.was_login_attempted()
                and not identity.get_identity_errors()):
            redirect(forward_url or '/', kw)

        if identity.was_login_attempted():
            if new_visit:
                msg = _(u"Cannot log in because your browser "
                         "does not support session cookies.")
            else:
                msg = _(u"The credentials you supplied were not correct or "
                         "did not grant access to this resource.")
        elif identity.get_identity_errors():
            msg = _(u"You must provide your credentials before accessing "
                     "this resource.")
        else:
            msg = _(u"Please log in.")
            if not forward_url:
                forward_url = request.headers.get('Referer', '/')

        # we do not set the response status here anymore since it
        # is now handled in the identity exception.
        return dict(logging_in=True, message=msg,
            forward_url=forward_url, previous_url=request.path_info,
            original_parameters=request.params)

    @expose()
    def logout(self):
        """Log out the current identity and redirect to start page."""
        identity.current.logout()
        redirect('/')

    @identity.require(identity.not_anonymous())
    @expose("auxmat.templates.seleciona_cadeiras")
    def sel_cad(self, *args, **kw):
        if kw.get("save_submit", None):
            keys=[]
            for key, value in kw.iteritems():
                if value=="on" and "Etapa" not in key:
                    keys.append(key)

            model.CadeiraUser.refresh_cads(identity.current.user.user_id, keys)

        cadeiras = model.Cadeira.query.all()
        lista_cadeiras = dict()
        for c in cadeiras:
            if c.etapa:
                etapa="Etapa %s"%c.etapa
            else:
                etapa="Sem Etapa"
            lista_cadeiras[etapa] = lista_cadeiras.get(etapa, None) and lista_cadeiras[etapa] + [(c.id, c.codigo, c.nome, c.creditos, c.marcado)] or [(c.id, c.codigo, c.nome, c.creditos, c.marcado)]
        lista_cadeiras = sorted(lista_cadeiras.iteritems(), key=lambda a: a[0])

        return dict(etapas=lista_cadeiras)



    @identity.require(identity.not_anonymous())
    @expose("auxmat.templates.blocos_busca")
    def blocos_busca(self, cadeiras_id=None, horarios_id=None, **kw):
        blocos=[]
        horarios = [
                time(7,30),
                time(8,30),
                time(9,30),
                time(10,30),
                time(11,30),
                time(12,30),
                time(13,30),
                time(14,30),
                time(15,30),
                time(16,30),
                time(17,30),
                time(18,30),
                time(19,30),
                time(20,30),
                time(21,30),
                time(22,30),
                ]
        dias = [u'Segunda', unicode('Terça', "UTF-8"), u'Quarta', u'Quinta', u'Sexta']

        cadeiras_id = eval(cadeiras_id)
    
        if cadeiras_id:
            cadeiras = [] 
            for cid in cadeiras_id:

                cadeiras.append(model.CadeiraSemestre.query.filter(model.CadeiraSemestre.cadeira_id==cid).all())

            pc = [[]]
            for turmas in cadeiras:
                newpcs = []
                for c in turmas:
                    newpc = [] 
                    for _pc in pc:
                        _newpc = copy(_pc)
                        _newpc.append((c))
                        newpc.append(_newpc)
                    newpcs.append(newpc)
                pc=[]
                for np in newpcs:
                    pc.extend(np)

            horario_comeco = horarios[-1]
            horario_fim    = horarios[0]

            for bloco in pc:
                new_bloco = {"horarios":{}, 
                             "turmas":[]}
                for dia in dias:
                    new_bloco["horarios"][dia]={}
                    for h in horarios:
                        new_bloco["horarios"][dia][h]=""
                pode = True
                for cadeira in bloco:
                    if pode:
                        for horario in cadeira.horarios:
                            if pode:
                                if new_bloco["horarios"][horario.horario.dia][horario.horario.inicio] == "":
                                    new_bloco["horarios"][horario.horario.dia][horario.horario.inicio] = cadeira.cadeira.codigo + cadeira.turma
                                    if horario.horario.inicio > horario_fim:
                                        horario_fim    = horario.horario.inicio
                                    if horario.horario.inicio < horario_comeco:
                                        horario_comeco = horario.horario.inicio
                                else:
                                    pode = False
                        new_bloco["turmas"].append(cadeira.id)
                if pode:
                    new_bloco["horario_comeco"]=horarios.index(horario_comeco)
                    new_bloco["horario_fim"]=horarios.index(horario_fim)
                    print new_bloco["horario_comeco"]
                    blocos.append(new_bloco)
                    


        elif horarios_id:
            pass

        else:
            pass
        return dict(possibilidades=blocos)


    @identity.require(identity.not_anonymous())
    @expose("auxmat.templates.bloco_atual")
    def bloco_atual(self, id, novo_bloco=None, **kw):
        if novo_bloco:
            novo_bloco = eval(novo_bloco)
            print novo_bloco
            bloco = model.Bloco.get(id)
            bloco.cadeiras = []
            
            for nb in novo_bloco:
                bloco.cadeiras.append(model.CadeiraSemestre.get(nb))
            bloco.save_or_update()
            bloco.flush()

        return dict(id=id)

    @identity.require(identity.not_anonymous())
    @expose("auxmat.templates.blocos")
    def lista_blocos(self, **kw):
        blocos = identity.current.user.blocos
        return dict(blocos=blocos)

    @identity.require(identity.not_anonymous())
    @expose("auxmat.templates.informacoes")
    def informacoes(self, bloco_id, **kw):
        cadeiras = model.Bloco.get(bloco_id).cadeiras
        return dict(cadeiras=cadeiras)

    @identity.require(identity.not_anonymous())
    @expose("auxmat.templates.monta_bloco")
    def monta_bloco(self, bloco_id=None, **kw):
        if not bloco_id:
            bloco = model.Bloco()
            bloco.user_user_id = identity.current.user.user_id
            bloco.save_or_update()
            bloco.flush()
            raise redirect("/monta_bloco/%s"%bloco.id)
        return dict(bloco_id=bloco_id)

    @identity.require(identity.not_anonymous())
    @expose("auxmat.templates.busca_cadeiras")
    def busca_cadeiras(self, **kw):
        cadeiras = [(c.id, c.codigo, c.nome, c.creditos) for c in model.Cadeira.cadeiras_possiveis(identity.current.user_id)]

        return dict(cadeiras=cadeiras)

