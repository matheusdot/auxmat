#coding: utf-8
"""This module contains the data model of the application."""

# symbols which are imported by "from auxmat.model import *"
__all__ = ['Group', 'Permission', 'User', 'Visit', 'VisitIdentity']

from datetime import datetime, time

import pkg_resources
pkg_resources.require('SQLAlchemy>=0.4.3')
pkg_resources.require('Elixir>=0.6.1')

# import the basic Elixir classes and functions for declaring the data model
# (see http://elixir.ematia.de/trac/wiki/TutorialDivingIn)
from elixir import Entity, Field, OneToMany, OneToOne, ManyToOne, ManyToMany, Boolean, Time
from elixir import options_defaults, using_options, setup_all
# import some datatypes for table columns from Elixir
# (see http://www.sqlalchemy.org/docs/04/types.html for more)
from elixir import String, Unicode, Integer, DateTime
from turbogears.database import get_engine, metadata, session
from turbogears import identity

options_defaults.update(autosetup=False, metadata=metadata, session=session)


class Visit(Entity):
    """A visit to your site."""

    using_options(tablename='visit')

    visit_key = Field(String(40), primary_key=True)
    created = Field(DateTime, nullable=False, default=datetime.now)
    expiry = Field(DateTime)

    @classmethod
    def lookup_visit(cls, visit_key):
        return Visit.get(visit_key)

    by_visit_key = lookup_visit


class VisitIdentity(Entity):
    """A Visit that is linked to a User object."""

    using_options(tablename='visit_identity')

    visit_key = Field(String(40), primary_key=True)
    user = ManyToOne('User', colname='user_id', use_alter=True)

    @classmethod
    def by_visit_key(cls, visit_key):
        """Look up VisitIdentity by given visit key."""
        return cls.get(visit_key)


class Group(Entity):
    """An ultra-simple Group definition."""

    using_options(tablename='tg_group')

    group_id = Field(Integer, primary_key=True)
    group_name = Field(Unicode(16), unique=True, nullable=False)
    display_name = Field(Unicode(255))
    created = Field(DateTime, default=datetime.now)
    users = ManyToMany('User', tablename='user_group')
    permissions = ManyToMany('Permission', tablename='group_permission')

    def __repr__(self):
        return '<Group: name="%s", display_name="%s">' % (
            self.group_name, self.display_name)

    def __unicode__(self):
        return self.display_name or self.group_name

    @classmethod
    def by_group_name(cls, group_name):
        """Look up Group by given group name."""
        return cls.get_by(group_name=group_name)

    by_name = by_group_name


class User(Entity):
    """Reasonably basic User definition.

    Probably would want additional attributes.

    """

    using_options(tablename='tg_user')

    user_id = Field(Integer, primary_key=True)
    user_name = Field(Unicode(16), unique=True, nullable=False)
    email_address = Field(Unicode(255), unique=True)
    display_name = Field(Unicode(255))
    _password = Field(Unicode(40), colname='password')
    created = Field(DateTime, default=datetime.now)
    groups = ManyToMany('Group', tablename='user_group')
    blocos = OneToMany("Bloco", inverse="user")
    cadeira_user = OneToMany("CadeiraUser", inverse="user")

    def __repr__(self):
        return '<User: name="%s", email="%s", display name="%s">' % (
            self.user_name, self.email_address, self.display_name)

    def __unicode__(self):
        return self.display_name or self.user_name

    @property
    def permissions(self):
        p = set()
        for g in self.groups:
            p |= set(g.permissions)
        return p

    @classmethod
    def by_email_address(cls, email_address):
        """Look up User by given email address.

        This class method that can be used to search users based on their email
        addresses since it is unique.

        """
        return cls.get_by(email_address=email_address)

    @classmethod
    def by_user_name(cls, user_name):
        """Look up User by given user name.

        This class method that permits to search users based on their
        user_name attribute.

        """
        return cls.get_by(user_name=user_name)

    by_name = by_user_name

    def _set_password(self, password):
        """Run cleartext password through the hash algorithm before saving."""
        self._password = identity.encrypt_password(password)

    def _get_password(self):
        """Returns password."""
        return self._password

    password = property(_get_password, _set_password)

#######################

class Bloco(Entity):
    using_options(tablename="bloco")

    id = Field(Integer, primary_key=True)
    user = ManyToOne("User", use_alter=True)
    cadeiras = ManyToMany("CadeiraSemestre", tablename="blocos_cadeiras")

class CadeiraUser(Entity):
    using_options(tablename="cadeiras_usuarios")

    id = Field(Integer, primary_key=True)
    status = Field(Unicode(255))
    user = ManyToOne("User", use_alter=True)
    cadeira = ManyToOne("Cadeira", use_alter=True)

    @classmethod
    def refresh_cads(cls, user_id, cadeiras):
        cads = cls.query.filter(cls.user_user_id==user_id).all()
        for cad in cads:
            if not cad.cadeira_id in cadeiras:
                cad.delete()
                cad.flush()
            else:
                cadeiras.remove(cad.cadeira_id)

        for cadeira in cadeiras:
            cad=CadeiraUser()
            cad.cadeira_id = cadeira 
            cad.user_user_id = user_id 
            cad.save_or_update()
            cad.flush()

class Cadeira(Entity):
    using_options(tablename="cadeira")

    id = Field(Integer, primary_key=True)
    codigo = Field(Unicode(255), unique=True)
    nome = Field(Unicode(255))
    etapa = Field(Integer)
    eletiva = Field(Boolean)
    creditos = Field(Integer)
    requisitos = ManyToMany("Cadeira", tablename="cadeiras_requisitos", remote_colname="requisito_id", local_colname="liberacao_id")
    creditos_requisito = Field(Integer)
    cadeira_user = OneToMany("CadeiraUser", inverse="cadeira")
    turmas = OneToMany("CadeiraSemestre", inverse="cadeira")

    @classmethod
    def liberacoes(cls, user_id, cadeiras_liberadoras):
        cadeiras_user = [c.id for c in cls.query.join(Cadeira.cadeira_user)\
                    .join(CadeiraUser.user)\
                    .filter(User.user_id == user_id)]
        creditos_user = sum([c.creditos for c in cls.query.join(Cadeira.cadeira_user)\
                    .join(CadeiraUser.user)\
                    .filter(User.user_id == user_id) if not c.eletiva])
        
        creditos_liberadores = creditos_user+sum([c.creditos for c in cls.query\
                    .filter(cls.id.in_(cadeiras_liberadoras)) if not c.eletiva])
       
        cadeiras = cls.query.join(Cadeira.turmas).all()
        cadeiras_possiveis = []
        for c in cadeiras:
            pode = False
            if not c.id in cadeiras_user and (not c.creditos_requisito or c.creditos_requisito < creditos_user):
                pode=True
                for r in c.requisitos:
                    if r.id not in cadeiras_user:
                        pode=False
            if not pode: 
                if not c.id in cadeiras_user and not c.id in cadeiras_liberadoras and (not c.creditos_requisito or c.creditos_requisito < creditos_liberadores):
                    pode=True
                    for r in c.requisitos:
                        if r.id not in cadeiras_liberadas:
                            pode=False
                if pode:
                    cadeiras_liberadas.append(c)

        return cadeiras_possiveis


    @classmethod
    def cadeiras_possiveis(cls, user_id):
        cadeiras_user = [c.id for c in cls.query.join(Cadeira.cadeira_user)\
                    .join(CadeiraUser.user)\
                    .filter(User.user_id == user_id)]
        creditos_user = sum([c.creditos for c in cls.query.join(Cadeira.cadeira_user)\
                    .join(CadeiraUser.user)\
                    .filter(User.user_id == user_id) if not c.eletiva])
       
        cadeiras = cls.query.join(Cadeira.turmas).all()
        cadeiras_possiveis = []
        for c in cadeiras:
            pode = False
            if not c.id in cadeiras_user and (not c.creditos_requisito or c.creditos_requisito < creditos_user):
                pode=True
                for r in c.requisitos:
                    if r.id not in cadeiras_user:
                        pode=False
            if pode:
                cadeiras_possiveis.append(c)


        return cadeiras_possiveis

    @property
    def marcado(self):
        if identity and identity.current and identity.current.user:
            a=CadeiraUser.query.filter(CadeiraUser.cadeira_id==self.id).filter(CadeiraUser.user_user_id==identity.current.user_id).all()
            if a:
                return "checked"
        return ""

    @classmethod
    def get_by_codigo(cls, codigo):
        inst = cls.query.filter(cls.codigo==codigo).first()
        if inst:
            return inst
        else:
            raise ValueError(u"Código não cadastrado no banco.", codigo)

class CadeiraSemestre(Entity):
    using_options(tablename="cadeira_semestre")

    id = Field(Integer, primary_key=True)
    professores = ManyToMany("Professor", tablename="professor_cadeira") 
    turma = Field(Unicode(255))
    vagas = Field(Integer)
    cadeira = ManyToOne("Cadeira", use_alter=True)
    horarios = OneToMany("CadeiraHorario", inverse="cadeira")
    blocos = ManyToMany("Bloco", tablename="blocos_cadeiras")

    @classmethod
    def get_by_horario_user(cls, dia, horario, bloco_id):
        turma = cls.query\
                .join(CadeiraSemestre.horarios)\
                .join(CadeiraHorario.horario)\
                .join(CadeiraSemestre.blocos)\
                .filter(Bloco.id==bloco_id)\
                .filter(Horario.dia==dia).filter(Horario.inicio==horario)\
                .first()
        return turma 


class CadeiraHorario(Entity):
    using_options(tablename="cadeiras_horarios")

    cadeira = ManyToOne("CadeiraSemestre", use_alter=True) 
    horario = ManyToOne("Horario", use_alter=True)
    local = Field(Unicode(255)) 

class Horario(Entity):
    using_options(tablename="horario")

    id = Field(Integer, primary_key=True)
    inicio = Field(Time)
    dia = Field(Unicode(255))
    cadeiras = OneToMany("CadeiraHorario", inverse="horario")

    @classmethod
    def get_by_horario(cls, dia, horario):
        inst = cls.query.filter(cls.dia==dia).filter(cls.inicio==horario).first()
        if inst:
            return inst
        else:
            raise ValueError(u"Código não cadastrado no banco.", codigo)

class Professor(Entity):
    using_options(tablename="professor")

    nome = Field(Unicode(255))
    cadeiras = ManyToMany("CadeiraSemestre", tablename="professor_cadeira")

    @classmethod
    def adiciona_cadeira(cls, professor, cadeira):
        p = cls.query.filter(cls.nome==professor.upper()).first()
        if not p:
            p = cls()
            p.nome = professor.upper()

        p.cadeiras = p.cadeiras and p.cadeiras + [cadeira] or [cadeira] 
        p.save_or_update()
        p.flush()

#######################


class Permission(Entity):
    """A relationship that determines what each Group can do."""

    using_options(tablename='permission')

    permission_id = Field(Integer, primary_key=True)
    permission_name = Field(Unicode(16), unique=True, nullable=False)
    description = Field(Unicode(255))
    groups = ManyToMany('Group', tablename='group_permission')

    def __repr__(self):
        return '<Permission: name="%s">' % self.permission_name

    def __unicode__(self):
        return self.permission_name

    @classmethod
    def by_permission_name(cls, permission_name):
        """Look up Permission by given permission name."""
        return cls.get_by(permission_name=permission_name)

    by_name = by_permission_name


# set up all Elixir entities declared above

setup_all()


# functions for populating the database

def bootstrap_model(clean=False, user=None):
    """Create all database tables and fill them with default data.

    This function is run by the 'bootstrap' function from the command module.
    By default it calls two functions to create all database tables for your
    model and optionally create a user.

    You can add more functions as you like to add more boostrap data to the
    database or enhance the functions below.

    If 'clean' is True, all tables defined by your model will be dropped before
    creating them again. If 'user' is not None, 'create_user' will be called
    with the given username.

    """
    create_tables(clean)
    if user:
        create_default_user(user)

def create_tables(drop_all=False):
    """Create all tables defined in the model in the database.

    Optionally drop existing tables before creating them.

    """
    get_engine()
    if drop_all:
        print "Dropping all database tables defined in model."
        from elixir import drop_all
        drop_all()
    from elixir import create_all
    create_all()

    print "All database tables defined in model created."

def create_default_user(user_name, password=None):
    """Create a default user."""
    try:
        u = User.by_user_name(user_name)
    except:
        u = None
    if u:
        print "User '%s' already exists in database." % user_name
        return
    from getpass import getpass
    from sys import stdin
    while password is None:
        try:
            password = getpass("Enter password for user '%s': "
                % user_name.encode(stdin.encoding)).strip()
            password2 = getpass("Confirm password: ").strip()
            if password != password2:
                print "Passwords do not match."
            else:
                password = password.decode(stdin.encoding)
                break
        except (EOFError, KeyboardInterrupt):
            print "User creation cancelled."
            return
    u = User()
    u.user_name = user_name
    u.display_name = u"Default User"
    u.email_address = u"%s@nowhere.xyz" % user_name
    u.password = password
    session.add(u)
    session.flush()
    print "User '%s' created." % user_name
