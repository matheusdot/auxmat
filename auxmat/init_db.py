#coding=utf-8
import urllib2
from BeautifulSoup import BeautifulSoup
from auxmat import command
import re
from datetime import datetime, time
command._read_config("dev.cfg")
command.bootstrap()

from auxmat import model

site_cadeiras = urllib2.urlopen("http://www1.ufrgs.br/PortalEnsino/GraduacaoCurriculos/plone.php?r=relatorio&curso=305&habilitacao=36&curriculo=95")
site_cadeiras = site_cadeiras.read()

site_horarios = urllib2.urlopen("https://www1.ufrgs.br/graduacao/ArquivosCompartilhados/Horariosvagas.php?tipo=gm", data="GM=38%2F1")
site_horarios = site_horarios.read() 

d = {"class":"sem-quebra"}

remove_endline = re.compile("\n *$")
re_codigo = re.compile("[A-Z]{3}[0-9]{5}")
re_req_cred = re.compile(u"&nbsp;&nbsp;- &nbsp;&nbsp;&nbsp;Créditos Obrigatórios - ")

def importa_cadeiras():
    bs = BeautifulSoup(site_cadeiras)

    for div in bs.body.div.form.findAll("div", d):
        print div.legend.string
        if "Etapa " in div.legend.string:
            etapa = int(div.legend.string.split(" ")[1])
        else:
            etapa = None
        if div.legend.string!=u"Liberações":
            for tr in div.findAll("tr")[1:]:
                td = tr.findAll("td")

                codigo = td[0].string
                nome = remove_endline.split(td[1].string or td[1].text.split("&nbsp;")[0])[0]
                creditos = int(td[3].string)

                if nome:
                    new_cadeira = model.Cadeira()
                    if codigo:
                        new_cadeira.codigo = codigo
                    elif u"TRABALHO DE GRADUAÇÃO" in nome:
                        new_cadeira.codigo = u"TCC00CIC"
                    new_cadeira.nome = nome
                    new_cadeira.creditos = creditos
                    if etapa:
                        new_cadeira.etapa = etapa

                    new_cadeira.save_or_update()
                    new_cadeira.flush()

                else:
                    print codigo, nome, creditos
                    raise Exception("hm")

def importa_requisitos():
    bs = BeautifulSoup(site_cadeiras)

    for div in bs.body.div.form.findAll("div", d):
        print div.legend.string
        if "Etapa " in div.legend.string:
            etapa = int(div.legend.string.split(" ")[1])
        else:
            etapa = None
        if div.legend.string!=u"Liberações":
            for tr in div.findAll("tr")[1:]:
                td = tr.findAll("td")

                codigo = td[0].string

                if codigo:
                    cadeira = model.Cadeira.get_by_codigo(codigo)
                elif u"TRABALHO DE GRADUAÇÃO" in td[1]:
                    cadeira = model.Cadeira.get_by_codigo(u"TCC00CIC")
                else:
                    raise Exception

                spans = td[1].findAll("span")
                if spans:
                    for span in spans:
                        if re_codigo.search(span.text):
                            cadeira.requisitos = cadeira.requisitos and cadeira.requisitos + [model.Cadeira.get_by_codigo(re_codigo.search(span.text).group())] or [model.Cadeira.get_by_codigo(re_codigo.search(span.text).group())]
                        elif re_req_cred.search(span.text):
                            cadeira.creditos_requisito = int(re_req_cred.split(span.text)[1])
                        else:
                            print span.text
                            raise Exception

                cadeira.save_or_update()
                cadeira.flush()

def init_horarios():
    dias = [u"Segunda",u"Terça",u"Quarta",u"Quinta",u"Sexta",u"Sábado"]
    horarios = [
            time(7,30),
            time(8,30),
            time(9,30),
            time(10,30),
            time(11,30),
            time(12,30),
            time(13,30),
            time(14,30),
            time(15,30),
            time(16,30),
            time(17,30),
            time(18,30),
            time(19,30),
            time(20,30),
            time(21,30),
            time(22,30),
            ]
    
    for dia in dias:
        for hora in horarios:
            new_horario = model.Horario()
            new_horario.inicio = hora
            new_horario.dia = dia
            new_horario.save_or_update()
            new_horario.flush()
    

def import_horarios():
    bs = BeautifulSoup(site_horarios)

    re_numero = re.compile("^[0-9]+")

    cadeira=None
    for tr in bs.table.tbody.findAll("tr"):
        td = tr.findAll("td")
        _codigo = td[0].text.replace("&nbsp;", "") 
        if _codigo != "":
            cadeira=model.Cadeira.get_by_codigo(re_codigo.search(_codigo).group())
        if not cadeira:
            raise Exception

        nova_turma = model.CadeiraSemestre()
        nova_turma.cadeira = cadeira

        nova_turma.turma = td[2].text
        nova_turma.vagas = int(td[3].text)

        nova_turma.save_or_update()
        nova_turma.flush()
        nova_turma.refresh()

        professores = [professor.text for professor in td[5].findAll("li")]

        for professor in professores:
            model.Professor.adiciona_cadeira(professor, nova_turma)
        
        horarios = [hor.text for hor in td[4].findAll("li", {"class":"hor"})]
        locais = [local.text for local in td[4].findAll("li", {"class":"espaco"})]
        
        for i, hor in enumerate(horarios):
            dia = hor.split(" ")[0]
            hora_inicio = hor.split(" ")[1].split("-")[0]
            creditos = int(re_numero.search(hor.split(" ")[2]).group())
            local = locais[i] 
            hora =datetime.strptime(hora_inicio, "%H:%M").time().hour
            for cred in range(0, creditos):
                horario = model.Horario.get_by_horario(dia, time(hora+cred,30))
                novo_horario = model.CadeiraHorario()
                novo_horario.cadeira = nova_turma
                novo_horario.local = local
                novo_horario.horario = horario
                novo_horario.save_or_update()
                novo_horario.flush()

