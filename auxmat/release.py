#coding=utf-8

short_name =       "auxmat"
package_name =     "auxmat"
name =             "AuxiliadorMatricula"
version =          "0.1.0"
description =      "Sistema para planejamento de cadeiras"
long_description = "Um sistema para que os alunos dos cursos de CIC e ECP da UFRGS possam planejar as suas cadeiras para os próximos semestres."
author =           "Matheus Marchini"
author_email =     "mmarchini@inf.ufrgs.br"
maintainer =       ""
maintainer_email=  ""
copyright =        ""
url =              ""
download_url =     ""
license =          "GPL"
#dev_status =       "Development Status :: 1 - Planning"
dev_status =       "Development Status :: 2 - Pre-Alpha"
#dev_status =       "Development Status :: 3 - Alpha"
#dev_status =       "Development Status :: 4 - Beta"
#dev_status =       "Development Status :: 5 - Production/Stable"
#dev_status =       "Development Status :: 6 - Mature"
#dev_status =       "Development Status :: 7 - Inactive"

zip_safe = False
keywords = []

install_requires = [
    "TurboGears==1.5.1",
    "SQLAlchemy==0.5.0",
    "html5lib==1.0b2",
    "AddOns==0.7",
    "BeautifulSoup==3.2.1",
    "BytecodeAssembler==0.6",
    "Cheetah==2.4.4",
    "CherryPy==3.2.4",
    "DecoratorTools==1.8",
    "Elixir==0.7.1",
    "Extremes==1.1.1",
    "FormEncode==1.2.6",
    "Genshi==0.7",
    "Markdown==2.3.1",
    "Paste==1.7.5.1",
    "PasteDeploy==1.5.0",
    "PasteScript==1.7.5",
    "SymbolType==1.0",
    "TGScheduler==1.6.3",
    "TurboJson==1.3.2",
    "WebOb==1.2.3",
    "WebTest==2.0.6",
    "argparse==1.2.1",
    "beautifulsoup4==4.2.1",
    "configobj==4.7.2",
    "distribute==0.6.24",
    "psycopg2==2.5.1",
    "python-dateutil==1.5",
    "simplejson==3.3.0",
    "six==1.3.0",
    "tgMochiKit==1.4.2",
    "waitress==0.8.5",
    "wsgiref==0.1.2",
]

classifiers = [
    dev_status,
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Topic :: Software Development :: Libraries :: Python Modules',
    'Framework :: TurboGears',
]

#test_suite='nose.collector'

entry_points = """
[console_scripts]
%(short_name)s = %(package_name)s.commands:start
"""%vars()

data_files = []
